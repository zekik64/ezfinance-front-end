import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchEntertainment, fetchHome, fetchWorld, fetchScience, fetchPolitics,
  fetchTechnology} from '../actions/index';
import { Link } from 'react-router';
import NewsCard from '../components/NewsCard';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import SearchInput, {createFilter} from 'react-search-input';
import Tooltip from 'react-tooltip-component';

const KEYS_TO_FILTERS = ['webTitle', 'subject', 'dest.name']


//Categories component that shows world, home, politics etc.
class Categories extends Component {

componentWillMount() {

  this.setState({searchTerm: ''});
  this.searchUpdated = this.searchUpdated.bind(this)

  // this.props.fetchGuardianNews();
  this.props.fetchEntertainment();
  this.props.fetchHome();
  this.props.fetchWorld();
  this.props.fetchScience();
  this.props.fetchPolitics();
  this.props.fetchTechnology();
}


renderNewsCards(newsItemsToRender) {


  if(newsItemsToRender.length == 0) {
    return(<h2> loading</h2>);
  } else {

    var filtered = newsItemsToRender.payload.response.results.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS));

    return filtered.map((item) => {
      const imageLocation = `${item.fields.thumbnail}`;
      const articleURL = `${item.webUrl}`;
      return (
        <NewsCard {...item} key={item.id}/>
      );
    });
  }
}

render() {
    return (

			<div>
        <Tooltip title="Enter a search term to search the articles on the page e.g. Brexit" position="bottom">
          <SearchInput className="search-input" onChange={this.searchUpdated} />
        </Tooltip>
  			<Tabs>
					<TabList>
						<Tab>Home</Tab>
						<Tab>World</Tab>
            <Tab>Politics</Tab>
            <Tab>Technology</Tab>
            <Tab>Science</Tab>
            <Tab>Entertainment</Tab>
					</TabList>

					<TabPanel>
						{this.renderNewsCards(this.props.sectionNews.homeNews)}
					</TabPanel>

					<TabPanel>
          {this.renderNewsCards(this.props.sectionNews.worldNews)}
					</TabPanel>

					<TabPanel>
          {this.renderNewsCards(this.props.sectionNews.politicsNews)}
					</TabPanel>

          <TabPanel>
          {this.renderNewsCards(this.props.sectionNews.technologyNews)}
          </TabPanel>

          <TabPanel>
          {this.renderNewsCards(this.props.sectionNews.scienceNews)}
          </TabPanel>

          <TabPanel>
          {this.renderNewsCards(this.props.sectionNews.entertainmentNews)}
          </TabPanel>

				</Tabs>
			</div>

    );
  }

  searchUpdated (term) {
  this.setState({searchTerm: term})
  }
}

function mapStateToProps(state) {
  return { sectionNews: state.sectionNews }
}


export default connect(mapStateToProps, { fetchEntertainment, fetchHome, fetchWorld,
  fetchScience, fetchPolitics, fetchTechnology })(Categories);
