import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchGuardianNews } from '../actions/index';
import { Link } from 'react-router';
import NewsCard from '../components/NewsCard';
import SearchInput, {createFilter} from 'react-search-input';
import Tooltip from 'react-tooltip-component';


const KEYS_TO_FILTERS = ['webTitle', 'subject', 'dest.name']


//the coompnent that handles the main index view of the website
class StoreIndex extends Component {

componentWillMount() {
  this.props.fetchGuardianNews();
  this.setState({searchTerm: ''});
  this.searchUpdated = this.searchUpdated.bind(this)
}

renderGuardianNews(newsItemsToRender) {

newsItemsToRender = newsItemsToRender.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS));

  return newsItemsToRender.map((item) => {
    return (
      <NewsCard {...item} key={item.id}/>
    );
  });
}


  render() {
  if (this.props.guardianNews.guardianNewsItems.length != 0) {
    return(
      <div>
        <Tooltip title="Enter a search term to search the articles on the page e.g. Brexit" position="bottom">
          <SearchInput className="search-input" onChange={this.searchUpdated} />
        </Tooltip>
        <div className="featuredProduct">
          <div className="row">
            {this.renderGuardianNews(this.props.guardianNews.guardianNewsItems.payload.response.results)}
          </div>
        </div>
      </div>
    );
  }
  else {
    return(
      <h2> Loading </h2>
    );
  }
}

  searchUpdated (term) {
  this.setState({searchTerm: term})
  }
}

function mapStateToProps(state) {
  return { guardianNews: state.guardianNews }
}


export default connect(mapStateToProps, { fetchGuardianNews })(StoreIndex);
