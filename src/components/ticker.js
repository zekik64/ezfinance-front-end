import React from 'react';
import { Link } from 'react-router';
import AnalogClock, { Themes } from 'react-analog-clock';

//this component contains the ticker and clock at the top of the website
const Ticker = () => {

    return(
      <div className="ticker">
        <p className="marquee">
          <span>Breaking News!</span>
        </p>
        <h1> News Corp </h1>
        <AnalogClock className="clock" theme={Themes.dark} width={150}/>
      </div>
    );
  }

  export default Ticker;
