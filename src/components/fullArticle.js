import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';


//the component that gives the user the fullArticle view
class FullArticle extends Component {

//defining an object on the FullArticle class
static contextTypes = {
  router: PropTypes.object
};

  componentWillMount() {
    this.goBack = this.goBack.bind(this)
  }

  render() {
    var item = null

    item = this.findMatchingArticle();

    return (
    <div>
      <button type="button" className="btn btn-primary backBtn pull-left col-sm-12 col-md-12" onClick={this.goBack}>Back</button>
      <div className="col-sm-12 col-md-12 pull-left" key={item.id}>
        <div className="fullArticle">
          <h2> {item.webTitle} </h2>
          <div className="thumbnail">
            <img src={item.fields.thumbnail} alt="No image available!" />
            <div className="caption" dangerouslySetInnerHTML={{__html: item.fields.body}}>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
  }

  findMatchingArticle() {
    var theItem = null;

    //check most popular first
    this.props.guardianNews.guardianNewsItems.payload.response.results.map((item, index) => {
      if (item.id == this.props.params.splat) {
          theItem = this.props.guardianNews.guardianNewsItems.payload.response.results[index];
        }
      });

    //check home news if nothing found
    if(theItem == null) {
      this.props.sectionNews.homeNews.payload.response.results.map((item, index) => {
        if (item.id == this.props.params.splat) {
            theItem = this.props.sectionNews.homeNews.payload.response.results[index];
          }
        });
    }

    //check worldNews news if nothing found
    if(theItem == null) {
      this.props.sectionNews.worldNews.payload.response.results.map((item, index) => {
        if (item.id == this.props.params.splat) {
            theItem = this.props.sectionNews.worldNews.payload.response.results[index];
          }
        });
    }

    //check politicsNews news if nothing found
    if(theItem == null) {
      this.props.sectionNews.politicsNews.payload.response.results.map((item, index) => {
        if (item.id == this.props.params.splat) {
            theItem = this.props.sectionNews.politicsNews.payload.response.results[index];
          }
        });
    }

    //check technologyNews news if nothing found
    if(theItem == null) {
      this.props.sectionNews.technologyNews.payload.response.results.map((item, index) => {
        if (item.id == this.props.params.splat) {
            theItem = this.props.sectionNews.technologyNews.payload.response.results[index];
          }
        });
    }

    //check scienceNews news if nothing found
    if(theItem == null) {
      this.props.sectionNews.scienceNews.payload.response.results.map((item, index) => {
        if (item.id == this.props.params.splat) {
            theItem = this.props.sectionNews.scienceNews.payload.response.results[index];
          }
        });
    }

    //check entertainmentNews news if nothing found
    if(theItem == null) {
      this.props.sectionNews.entertainmentNews.payload.response.results.map((item, index) => {
        if (item.id == this.props.params.splat) {
            theItem = this.props.sectionNews.entertainmentNews.payload.response.results[index];
          }
        });
    }

    return theItem;

  }

  //function that makes the app go back to the last page
  goBack () {
  this.context.router.goBack()
  }
}

function mapStateToProps(state) {
  return {
            guardianNews: state.guardianNews,
            sectionNews: state.sectionNews
         }
}

// connect: first argument is mapStateToProps, 2nd is mapDispatchToProps
export default connect(mapStateToProps, {})(FullArticle);
