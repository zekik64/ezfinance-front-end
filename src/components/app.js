import React, { Component } from 'react';
import { Router, Route, Link } from 'react-router'
import Navbar from '../components/navbar';
import Ticker from '../components/ticker';


//The main app component that loads in other components as and when required.
export default class App extends Component {

  render() {
    return (
      <div>
        <Navbar />
        <Ticker />
        <div className='body'>
          {this.props.children}
        </div>

        <footer>
          Copyright News Corp
        </footer>
      </div>
    );
  }
}
