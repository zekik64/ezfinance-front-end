import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import NewsCard from '../components/NewsCard';

//this component is responsible for showing the saved news articles
class Saved extends Component {

componentWillMount() {
}

renderSavedArticles(newsItemsToRender) {

  return newsItemsToRender.map((item) => {
    return (
      <NewsCard {...item.payload} key={item.payload.id} />
    );
  });
}


  render() {
  if (this.props.savedArticles.articles.length != 0) {
    return(
      <div>
        <div className="featuredProduct">
          <div className="row saved">
            {this.renderSavedArticles(this.props.savedArticles.articles)}
          </div>
        </div>
      </div>
    );
  }
  else {
    return(
      <h2> You have no saved articles! </h2>
    );
  }
}

}

function mapStateToProps(state) {
  return { savedArticles: state.savedArticles }
}


export default connect(mapStateToProps, { })(Saved);
