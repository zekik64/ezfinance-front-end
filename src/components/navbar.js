import React from 'react';
import { Link } from 'react-router';
var $ = require("jquery");

//navbar component that allows the user to navigate
const Navbar = () => {

//jquery to handle the active tab on the navbar
$(document).ready(function() {
  $( "#mostPopular" ).click(function() {
    $("#mostPopular").addClass( "active" );
    $("#guardian").removeClass( "active" );
    $("#saved").removeClass( "active" );
  });

  $( "#guardian" ).click(function() {
    $("#guardian").addClass( "active" );
    $("#mostPopular").removeClass( "active" );
    $("#saved").removeClass( "active" );
  });

  $( "#saved" ).click(function() {
    $("#saved").addClass( "active" );
    $("#mostPopular").removeClass( "active" );
    $("#guardian").removeClass( "active" );
  });
});

    return(
      <header>
        <nav className="navbar navbar-dark bg-inverse">
          <ul className="nav navbar-nav" id="navList">
            <li className="nav-item active" id="mostPopular">
              <Link to="/~B00637176/workspace/COM554/assignment_2/" className="nav-link" id="mostPopularLink">Home / Most Popular</Link>
            </li>
            <li className="nav-item" id="guardian">
              <Link to="/~B00637176/workspace/COM554/assignment_2/Guardian" id="guardianLink" className="nav-link">The Guardian</Link>
            </li>
            <li className="nav-item" id="saved">
              <Link to="/~B00637176/workspace/COM554/assignment_2/Saved" className="nav-link">Saved Articles</Link>
            </li>
          </ul>
        </nav>
      </header>
    );
  }

  export default Navbar;
