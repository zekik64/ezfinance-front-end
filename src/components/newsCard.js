import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { saveArticle } from '../actions/index';

//the newscard component which is responsible for showing each of the news articles
class NewsCard extends Component {

  componentWillMount() {
    this.saveArticle = this.saveArticle.bind(this)

  }



  render() {
    var currentArticle = this.props;
    var mostPopularUrl = '/~B00637176/workspace/COM554/assignment_2/mostPopular/' + this.props.id;
    return (
    <div className="col-sm-6 col-md-3 pull-left" key={this.props.id}>
      <div className="newsCard">
        <Link to= {mostPopularUrl}>
          <div className="thumbnail">
            <img src={this.props.fields.thumbnail} alt="No image available" />
            <div className="caption">
              <p>{this.props.webTitle}</p>
              </div>
          </div>
        </Link>
        <div className="saveArticle">
          <button type="button" className="btn btn-primary" onClick={this.saveArticle}>Save</button>
        </div>
      </div>
    </div>
  );
  }

  saveArticle() {
    this.props.saveArticle(this.props);
  }
}

function mapStateToProps(state) {
  return { guardianNews: state.guardianNews }
}

// connect: first argument is mapStateToProps, 2nd is mapDispatchToProps
export default connect(mapStateToProps, {saveArticle})(NewsCard);
