import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/app';
import NewsIndex from './components/news_index';
import NewsCard from './components/NewsCard';
import Categories from './components/categories'
import FullArticle from './components/FullArticle';
import Saved from './components/saved';


//sets up all of the routes of the website
export default (
<Route path="/~B00637176/workspace/COM554/assignment_2/" component={App}>
  <IndexRoute component={NewsIndex} />
  <Route path="/~B00637176/workspace/COM554/assignment_2/mostPopular/*" component={FullArticle} />
  <Route path="/~B00637176/workspace/COM554/assignment_2/Guardian" component={Categories} />
  <Route path="/~B00637176/workspace/COM554/assignment_2/Saved" component={Saved} />
</Route>
);
