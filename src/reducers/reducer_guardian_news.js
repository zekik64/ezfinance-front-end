import { FETCH_GUARDIAN_NEWS } from '../actions/index';

const INITIAL_STATE = { guardianNewsItems: [] };

//this reducer stores and updates the most popular news items
export default function(state = INITIAL_STATE, action) {

  switch (action.type) {
    case FETCH_GUARDIAN_NEWS:
      return {
        ...state,
        guardianNewsItems: action
      };
      break;
    default:
      return state;

  }
}
