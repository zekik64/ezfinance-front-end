import { SAVE_ARTICLE } from '../actions/index';

const INITIAL_STATE = { articles: [] };

//this reducer stores and updates the most saved news items
export default function(state = INITIAL_STATE, action) {

  switch (action.type) {
    case SAVE_ARTICLE:
      return {
        ...state,
        articles: [...state.articles, action]
      };
      break;
    default:
      return state;

  }
}
