import { combineReducers } from 'redux';
import guardianNewsReducer from './reducer_guardian_news';
import sectionsNewsReducer from './reducer_section_news';
import savedArticlesReducer from './reducer_saved_news';
import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
  guardianNews: guardianNewsReducer,
  sectionNews: sectionsNewsReducer,
  savedArticles: savedArticlesReducer
});

export default rootReducer;
