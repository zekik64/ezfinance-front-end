import { FETCH_HOME_NEWS, FETCH_WORLD_NEWS, FETCH_POLITICS_NEWS,
  FETCH_SCIENCE_NEWS, FETCH_TECHNOLOGY_NEWS, FETCH_ENTERTAINMENT_NEWS } from '../actions/index';

const INITIAL_STATE = { homeNews: [], worldNews: [], politicsNews: [], scienceNews: [],
 technologyNews: [], entertainmentNews: [] };

 //this reducer stores and updates the category news items
export default function(state = INITIAL_STATE, action) {

  switch (action.type) {
    case FETCH_HOME_NEWS:
      return {
        ...state,
        homeNews: action
      };
      break;
    case FETCH_WORLD_NEWS:
      return {
        ...state,
        worldNews: action
      };
      break;
    case FETCH_POLITICS_NEWS:
      return {
        ...state,
        politicsNews: action
      };
      break;
    case FETCH_SCIENCE_NEWS:
      return {
        ...state,
        scienceNews: action
      };
      break;
    case FETCH_TECHNOLOGY_NEWS:
      return {
        ...state,
        technologyNews: action
      };
      break;
    case FETCH_ENTERTAINMENT_NEWS:
      return {
        ...state,
        entertainmentNews: action
      };
      break;
    default:
      return state;

  }
}
