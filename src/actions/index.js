//this file contains all of the actions of my react app
//Actions are payloads of information that send data from your application to your store.
//They are the only source of information for the store
//My store is Redux
import axios from 'axios';
var $ = require("jquery");


//actions
export const FETCH_GUARDIAN_NEWS = 'FETCH_GUARDIAN_NEWS';
export const FETCH_HOME_NEWS = 'FETCH_HOME_NEWS';
export const FETCH_WORLD_NEWS = 'FETCH_WORLD_NEWS';
export const FETCH_POLITICS_NEWS = 'FETCH_POLITICS_NEWS';
export const FETCH_TECHNOLOGY_NEWS = 'FETCH_TECHNOLOGY_NEWS';
export const FETCH_SCIENCE_NEWS = 'FETCH_SCIENCE_NEWS';
export const FETCH_ENTERTAINMENT_NEWS = 'FETCH_ENTERTAINMENT_NEWS';
export const SAVE_ARTICLE = 'SAVE_ARTICLE';


//urls
const GUARDIAN_URL = 'https://content.guardianapis.com/search?api-key=590864f1-89d0-43c8-a4d3-b819b631a88a&show-fields=body,thumbnail&page-size=12';
const ENTERTAINMENT_URL = 'https://content.guardianapis.com/film?api-key=590864f1-89d0-43c8-a4d3-b819b631a88a&show-fields=body,thumbnail&page-size=12'
const HOME_URL = 'https://content.guardianapis.com/housing-network?api-key=590864f1-89d0-43c8-a4d3-b819b631a88a&show-fields=body,thumbnail&page-size=12'
const WORLD_URL = 'https://content.guardianapis.com/world?api-key=590864f1-89d0-43c8-a4d3-b819b631a88a&show-fields=body,thumbnail&page-size=12'
const POLITICS_URL = 'https://content.guardianapis.com/politics?api-key=590864f1-89d0-43c8-a4d3-b819b631a88a&show-fields=body,thumbnail&page-size=12'
const SCIENCE_URL = 'https://content.guardianapis.com/science?api-key=590864f1-89d0-43c8-a4d3-b819b631a88a&show-fields=body,thumbnail&page-size=12'
const TECHNOLOGY_URL = 'https://content.guardianapis.com/technology?api-key=590864f1-89d0-43c8-a4d3-b819b631a88a&show-fields=body,thumbnail&page-size=12'

//functions using ajax
export function fetchGuardianNews() {
 return dispatch => {
   $.ajax({
        url: GUARDIAN_URL,
        method: 'GET',
      }).done(function(result) {
        dispatch(setNewsData(result)); // Use a normal function to set the received state
      }).fail(function(err) {
        throw err;
    });
 }
}

export function fetchEntertainment() {
  return dispatch => {
    $.ajax({
         url: ENTERTAINMENT_URL,
         method: 'GET',
       }).done(function(result) {
         dispatch(setEntertainmentData(result)); // Use a normal function to set the received state
       }).fail(function(err) {
         throw err;
     });
  }
}

export function fetchHome() {
  return dispatch => {
    $.ajax({
         url: HOME_URL,
         method: 'GET',
       }).done(function(result) {
         dispatch(setHomeData(result)); // Use a normal function to set the received state
       }).fail(function(err) {
         throw err;
     });
  }
}

export function fetchWorld() {
  return dispatch => {
    $.ajax({
         url: WORLD_URL,
         method: 'GET',
       }).done(function(result) {
         dispatch(setWorldData(result)); // Use a normal function to set the received state
       }).fail(function(err) {
         throw err;
     });
  }
}

export function fetchPolitics() {
  return dispatch => {
    $.ajax({
         url: POLITICS_URL,
         method: 'GET',
       }).done(function(result) {
         dispatch(setPoliticsData(result)); // Use a normal function to set the received state
       }).fail(function(err) {
         throw err;
     });
  }
}

export function fetchScience() {
  return dispatch => {
    $.ajax({
         url: SCIENCE_URL,
         method: 'GET',
       }).done(function(result) {
         dispatch(setScienceData(result)); // Use a normal function to set the received state
       }).fail(function(err) {
         throw err;
     });
  }
}

export function fetchTechnology() {
  return dispatch => {
    $.ajax({
         url: TECHNOLOGY_URL,
         method: 'GET',
       }).done(function(result) {
         dispatch(setTechnologyData(result)); // Use a normal function to set the received state
       }).fail(function(err) {
         throw err;
     });
  }
}

function setNewsData(data) {
  return {
    type: FETCH_GUARDIAN_NEWS,
    payload: data
  };
}

function setEntertainmentData(data) {
  return {
    type: FETCH_ENTERTAINMENT_NEWS,
    payload: data
  };
}

function setHomeData(data) {
  return {
    type: FETCH_HOME_NEWS,
    payload: data
  };
}

function setWorldData(data) {
  return {
    type: FETCH_WORLD_NEWS,
    payload: data
  };
}

function setPoliticsData(data) {
  return {
    type: FETCH_POLITICS_NEWS,
    payload: data
  };
}

function setScienceData(data) {
  return {
    type: FETCH_SCIENCE_NEWS,
    payload: data
  };
}

function setTechnologyData(data) {
  return {
    type: FETCH_TECHNOLOGY_NEWS,
    payload: data
  };
}

export function saveArticle(article) {
  return {
    type: SAVE_ARTICLE,
    payload: article
  };
}
